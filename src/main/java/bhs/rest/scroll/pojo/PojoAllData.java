package bhs.rest.scroll.pojo;

import bhs.db.domain.Scroll;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PojoAllData {
    private List<Scroll> data;
    private Scroll published;
}
