package bhs.rest.scroll.pojo;

import lombok.Data;

@Data
// @NoArgsConstructor
// @AllArgsConstructor
public class PojoSelected {
    private Long selected;
}
