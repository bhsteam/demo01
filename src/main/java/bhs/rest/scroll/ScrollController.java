package bhs.rest.scroll;

import bhs.db.domain.Scroll;
import bhs.db.repo.ScrollRepository;
import bhs.rest.scroll.pojo.PojoAllData;
import bhs.rest.scroll.pojo.PojoSelected;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("api/scroll")
public class ScrollController {
    private final ScrollRepository scrollRepository;
    private final StateService stateService;

    public ScrollController(ScrollRepository scrollRepository, StateService stateService) {
        this.scrollRepository = scrollRepository;
        this.stateService = stateService;
    }

    @PostMapping("load")
    public PojoAllData load() {
        PojoAllData data = new PojoAllData();
        data.setData(scrollRepository.findAll());
        data.setPublished(stateService.getSelected().get());
        return data;
    }

    @PostMapping("select")
    public PojoSelected select(@RequestBody PojoSelected selected) {
        log.debug("select: {}", selected);
        Optional<Scroll> searchResult = scrollRepository.findById(selected.getSelected());
        searchResult.ifPresent(scroll -> stateService.getSelected().set(scroll));
        return selected;
    }

    @PostMapping("delete")
    public PojoAllData delete(@RequestBody PojoSelected selected) {
        log.debug("delete: {}", selected);
        scrollRepository.deleteById(selected.getSelected());
        return this.load();
    }

    @PostMapping("edit")
    public PojoAllData edit(@RequestBody Scroll scroll) {
        log.debug("edit: {}", scroll);
        log.debug((scroll.getId() == null) ? "create new" : "update");
        scrollRepository.save(scroll);
        return this.load();
    }

}
