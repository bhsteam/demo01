package bhs.rest.scroll;

import bhs.db.domain.Scroll;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicReference;

@Data
@Slf4j
@Service
@Scope("singleton")
public class StateService {
    private AtomicReference<Scroll> selected = new AtomicReference<>();

    public StateService() {
        log.debug("StateService");
        Scroll scroll = Scroll.builder()
                .id((long) 0)
                .fio("")
                .descr("")
                .build();
        selected.set(scroll); // set default
    }
}