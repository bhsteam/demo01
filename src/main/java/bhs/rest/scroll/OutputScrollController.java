package bhs.rest.scroll;

import bhs.db.domain.Scroll;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/out")
public class OutputScrollController {

    private final StateService stateService;

    public OutputScrollController(StateService stateService) {
        this.stateService = stateService;
    }

    @GetMapping("scroll")
    public List<Scroll> getOutScroll() {
        List<Scroll> result = new ArrayList<>();
        result.add(stateService.getSelected().get());
        return result;
    }
}

