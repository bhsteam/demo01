package bhs.security.jwt;

import org.springframework.security.core.AuthenticationException;

class InvalidJwtAuthException extends AuthenticationException {
    InvalidJwtAuthException(String e) {
        super(e);
    }
}
