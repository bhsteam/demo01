package bhs;

import bhs.configuration.AppConfig;
import bhs.db.domain.Scroll;
import bhs.db.domain.User;
import bhs.db.repo.ScrollRepository;
import bhs.db.repo.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Slf4j
@Component
public class DataInitializer implements CommandLineRunner {
    private final ScrollRepository scrollRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final AppConfig appConfig;

    public DataInitializer(UserRepository userRepository, ScrollRepository scrollRepository, PasswordEncoder passwordEncoder, AppConfig appConfig) {
        this.scrollRepository = scrollRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.appConfig = appConfig;
    }

    private void addTestUsers() {
/*
        this.userRepository.save(User.builder()
                .username("user")
                .password(this.passwordEncoder.encode("password"))
                .roles(Arrays.asList("ROLE_USER"))
                .build()
        );
*/
        this.userRepository.save(User.builder()
                .username(appConfig.getLogin())
                .password(this.passwordEncoder.encode(appConfig.getPass()))
                .roles(Arrays.asList("ROLE_USER", "ROLE_ADMIN"))
                .build()
        );

        log.debug("Users:");
        this.userRepository.findAll().forEach(v -> log.debug("User: " + v.toString()));
    }

    private void addTestScrolls() {
        scrollRepository.save(Scroll.builder()
                .fio("Вася Пупкин")
                .descr("Some unknown guy")
                .build());

        scrollRepository.save(Scroll.builder()
                .fio("Петя Васюткин")
                .descr("Some popular guy")
                .build());

        scrollRepository.findAll().forEach(v -> log.debug("Scroll: " + v.toString()));
    }

    @Override
    public void run(String... args) {
        if (userRepository.count() == 0) {
            this.addTestUsers();
            this.addTestScrolls();
        }
        log.debug(" ------------------ DataInitializer.done ------------------ ");

    }


}
//