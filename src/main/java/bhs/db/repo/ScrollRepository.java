package bhs.db.repo;

import bhs.db.domain.Scroll;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScrollRepository extends JpaRepository<Scroll, Long> {
}
